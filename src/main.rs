extern crate rand;
use rand::Rng;

struct GameData {
	canteen: u32,
	thirst: u32,// Dead at 6
	travelled: i32,
	cannibals: i32, // Move rand 7-14 // close at less than 15
	bike: u32, // Tired at 5, dead at 6
}

impl GameData {
	// Initialise
	fn new() -> GameData {
			GameData {
				canteen: 5,
				thirst: 0,
				travelled: 0,
				cannibals: -20,
				bike: 0,
		}
	}
	// Have a drink
	fn have_drink(&mut self) {
		if self.canteen >= 1 {
			println!("You take a drink from your canteen");
			self.canteen -= 1;
			self.thirst = 0;
		} else {
			println!("Your canteen is empty");
		}
	}
	fn forward_moderate(&mut self) {
		println!("You pedal at a steady pace");
		self.travelled += rand::thread_rng().gen_range(5,12);
		self.cannibals += rand::thread_rng().gen_range(7,14);
		self.bike += rand::thread_rng().gen_range(0,1);
		self.thirst += 1;
	}
	fn forward_fulltilt(&mut self) {
		println!("You pump the pedals hard on you bike and speed ahead");
		self.travelled += rand::thread_rng().gen_range(10,20);
		self.cannibals += rand::thread_rng().gen_range(7,14);
		self.bike += rand::thread_rng().gen_range(1,3);
		self.thirst += 1;
	}
	fn stop_rest(&mut self) {
		println!("You stop for a rest and repair your bike");
		self.bike = 0;
		self.cannibals += rand::thread_rng().gen_range(7,14);
	}
	fn check_stat(&self) {
		println!("You have {} drinks in your canteen", self.canteen);
		println!("You have travelled {} kilometers", self.travelled);
		println!("The cannibals are {} kilometers behind you", self.travelled - self.cannibals);
	}
	fn game_check(&mut self) -> bool {
		if self.travelled >= 200 {
			println!("You made it through the city\nYou win!");
			return true;
		} else if self.thirst > 6 {
			println!("You have died of thirst\nGame Over!");
			return true;
		} else if self.travelled <= self.cannibals {
			println!("The cannibals caught you\nGame Over!");
			return true;
		} else if self.bike >= 8 {
			println!("Your bike has broken, the cannibals will catch you easily\nGame Over!");
			return true;
		} else {
			if self.thirst > 4 {
				println!("You are thirsty");
			}
			if self.bike > 5 {
				println!("Your bike needs maintenence");
			}
			if self.travelled - self.cannibals <= 15 {
				println!("The cannibals are getting close");
			}
			if rand::thread_rng().gen_range(1,20) == 1 {
				println!("You found some clean water to refill your canteen");
				self.canteen = 5;
			}
			return false;
		}
	}
}

fn print_menu() {
	println!(
	"\n	A. Drink from your canteen
	B. Pedal at a steady pace
	C. Pedal hard and speed up
	D. Stop and rest
	E. Check how you are doing
	Q. Quit");
}

fn main() {
    println!(
	"Welcome to Rusty Bike!
You have stolen a bike to make your way across the ravaged city.
The cannibals want their bike back.
Survive the city and outrun the cannibals");
    
    let mut this_game = GameData::new();
    
    let mut input: char = 'x';
    
    while input != 'q' {
    	print_menu();
    	println!("\nYour choice: ");
    	// Grab string input, convert to char.
    	let mut temp = String::new();
    	std::io::stdin().read_line(&mut temp).ok().expect("Input failed");
    	input = temp.chars().next().unwrap() as char;
    	
    	if input == 'a' || input == 'A' {
			this_game.have_drink();
    	}
    	if input == 'b' || input == 'B' {
    		this_game.forward_moderate();
    	}
    	if input == 'c' || input == 'C' {
    		this_game.forward_fulltilt();
    	}
    	if input == 'd' || input == 'D' {
    		this_game.stop_rest();
    	}
    	if input == 'e' || input == 'E' {
    		this_game.check_stat();
    	}
    	if this_game.game_check() == true {
    		break
    	}
    }
}
